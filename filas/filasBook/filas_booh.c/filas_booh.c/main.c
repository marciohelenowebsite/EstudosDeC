//
//  main.c
//  filas_booh.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

/* This program simulates Circular Queue operations */
#include <stdio.h>
//#include <conio.h>

int addQ (int cirQ[],int *Front, int *Rear, int val, int size);
int delQ (int cirQ[], int *Front, int *Rear, int size);
void display (int cirQ[], int Front, int Rear, int size);

void main()
{
    int cirQ[20];   /* the queue declaration */
    int Front;
    int Rear;
    
    int val;
    int size;
    int choice;
    int result;
    Front = Rear=0;   /* initially the queue set to be empty */
    printf (“\n Enter the size of the queue”);
    scanf (“%d”, & size);
    /* create menu */
    do
    {
        clrscr();
        printf (“\n Menu – Circular queue operations”);
        printf (“\n Add              1”);
        printf (“\n Delete             2”);
        printf (“\n Display queue      3”);
        printf (“\n Quit        4”);
        
        printf (“\n Enter your choice:”);
        scanf (“%d”, & choice);
        
        switch (choice)
        {
            case 1: printf (“\n Enter the value to be added”);
                scanf (“%d”, & val);
                result = addQ(cirQ, &Front, &Rear, val, size);
                if (result == 0)
                    printf (“\n The queue full”);
                break;
            case 2: result = delQ(cirQ, &Front, &Rear, size);
                if (result == 9999)
                    printf (“\n The queue is Empty”);
                else
                    printf (“\n The deleted value = %d”, result);
                break;
            case 3: display (cirQ, Front, Rear, size);
                break;
        }
        printf(“\n\n Press any key to continue”);
        getch();
    }
    while (choice != 4);
}

int addQ (int cirQ[],int * Front, int *Rear, int val, int size)
{
    if ( ((*Rear + 1) % size) == *Front)
        return 0; /* the queue is Full */
    else
    {
        *Rear= (*Rear + 1) % size;
        cirQ[*Rear]= val;
        return 1;
    }
}

int delQ (int cirQ[], int *Front, int *Rear, int size)

{int val;
    if (*Front == *Rear)
        return 9999; /* the queue is empty */
    else
    {
        *Front =(*Front + 1) % size;
        val = cirQ[*Front];
        
        return val;
    }
}

void display (int cirQ[], int Front, int Rear, int size)
{
    int i;
    printf (“\n The contents of queue are:”);
    i=Front;
    while ( i != Rear)
    {
        i = (i + 1) % size;
        printf (“%d ”, cirQ[i]);
    }
}
