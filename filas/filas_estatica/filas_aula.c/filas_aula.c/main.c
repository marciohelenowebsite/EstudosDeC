//
//  main.c
//  filas_aula.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include "FilaEstatica.h"


int main(){
    Fila f;
    int opcao, res, num;
    int *aux;
    
    inicialize(&f);
    
    do{
        printf("\nInforme uma das opcoes abaixo:\n");
        printf("1-Inserir na fila\n2-Remover da fila\n3-Listar fila\n4-Sair");
        printf("\n\nDigite sua opcao:");
        scanf("%d",&opcao);
        
        switch(opcao){
            case 1:
                printf("Informe um inteiro:");
                scanf("%d",&num);
                res = enqueue(&f,num);
                if(res == 0){
                    printf("\nFila cheia!\n");
                }else{
                    printf("\nInteiro %d inserido com sucesso!\n",num);
                }
                break;
            case 2:
                aux = dequeue(&f);
                if(aux == NULL){
                    printf("\nFila vazia!\n");
                }else{
                    printf("\nNumero %d removido da fila com suceso!",*aux);
                }
                break;
            case 3:
                res = listQueue(&f);
                if(res == 0){
                    printf("\nFila vazia!\n");
                }
                break;
            case 4:
                printf("Fim do programa!");
            default:
                printf("Opcao invalida!Tente novamente!");
                
        }
        
    }while(opcao!=4);
    
    
}
