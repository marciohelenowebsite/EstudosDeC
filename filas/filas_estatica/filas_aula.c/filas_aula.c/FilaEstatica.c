//
//  FilaEstatica.c
//  filas_aula.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include "FilaEstatica.h"


struct fila{
    int elementos[TAM];
    int inicio,fim;
    int qtd; //Quantidade de elementos na fila
};
typedef struct fila Fila;

void inicialize(Fila *f){
    f->inicio =0;
    f->fim = -1;
    f->qtd = 0;
}

int isEmptyQueue(Fila *f){
    if(f->qtd == 0){
        return 1;
    }else{
        return 0;
    }
}

int isFullQueue(Fila *f){
    if(f->qtd == TAM){
        return 1;
    }else{
        return 0;
    }
}

int enqueue(Fila *f,int info){
    if(isFullQueue(f) == 1){
        return 0;
    }else{
        if(f->fim == TAM - 1){
            f->fim = 0;
        }else{
            f->fim++;
        }
        f->elementos[f->fim] =info;
        f->qtd++;
        return 1;
    }
}

int* dequeue(Fila *f){
    if(isEmptyQueue(f) == 1){
        return NULL;
    }else{
        int *removido;
        removido = &f->elementos[f->inicio];
        
        if(f->inicio == TAM - 1){
            f->inicio = 0;
        }else{
            f->inicio++;
        }
        f->qtd--;
        return removido;
    }
    
}

int listQueue(Fila *f){
    int i;
    
    if(isEmptyQueue(f) == 1){
        return 0;
    }else{
        printf("**********Elementos da Fila**********\n");
        if(f->inicio <= f->fim){
            for(i=f->inicio;i<=f->fim;i++){
                printf("%d ",f->elementos[i]);
            }
        }else{
            for(i=f->inicio;i<=TAM-1;i++){
                printf("%d ",f->elementos[i]);
            }
            for(i=0;i<=f->fim;i++){
                printf("%d ",f->elementos[i]);
            }
            
        }
        return 1;
    }
}
