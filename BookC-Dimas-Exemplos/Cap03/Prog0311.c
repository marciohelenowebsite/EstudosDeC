#include "stdio.h"

int main() {

  /*
   * Implemente um programa que, dada uma letra, indique qual o estado civil
   * de uma pessoa
   */

  char estadoCivil;
  
  printf("Informe seu estado civil. \n");
  scanf("%s", &estadoCivil);

  if (estadoCivil == 'C' || estadoCivil == 'c') {
    printf("Casado \n");
  } else if(estadoCivil == 'S' || estadoCivil == 's') {
    printf("Solteiro \n");
  } else if(estadoCivil == 'V' || estadoCivil == 'v') {
    printf("Viúvo \n");
  } else if (estadoCivil == 'D' || estadoCivil == 'd') {
    printf("Divorciado \n");   
  } else {
    printf("Estado Civil Inválido \n");
  }
  

}
