#include "stdio.h"

int main() {
  /*
   * Escreva um programa que solicite um sálario ao ultilizador e mostre
   * o imposto a pagar
   * Se o sálario for negativo mostre um erro respectivo
   * Se o salário for maior que 1000, pague 10% de imposto, se não paga apenas 5%
   *
   */

  float salario;

  printf("Informe seu salário :\n");
  scanf("%f", &salario);

  if (salario <= 0) {
    printf("O valor informado não e válido.\n");
    printf("Informe um valor válido\n");
  } else {
    if (salario > 1000) {
      printf("Seu salário é de %.2f e vc pagará 10 por cento que dará um total de imposto R$ %.2f\n", salario, salario * 0.10);
    } else{
      printf("Seu salário é de %.2f e vc pagará 5 por cento que dará um total de imposto R$ %.2f\n", salario, salario * 0.05);

    }
  }
}
