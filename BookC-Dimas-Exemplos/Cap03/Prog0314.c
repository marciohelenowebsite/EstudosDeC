#include <stdio.h>
#include "string.h"
int main() 
{
  /*
   * Escreva um programa que calcule o imposto pago por mulheres e por homens,
   * sabendo que as mulheres pagam 10% e os homens pagam mais 5% do que as 
   * mulheres.
   */

  float salario;
  char sexo;
  float imposto;

  printf("Impostometro, gostaria de saber o valor do seu imposto? \t\n");
  printf("Informe seu salário: \t\n");
  scanf("%f", &salario);

  printf("Informe seu Sexo.: \t\n");
  scanf(" %c", &sexo);

  if (sexo == 'f' || sexo == 'F') {
    imposto = salario * 0.10;
    printf("O valor de imposto pago é de %.2f\t\n", imposto);
  } else if ( sexo == 'm' || sexo == 'M') {
    imposto = salario * 0.15;
    printf("O valor de imposto pago é de %.2f\t\n", imposto);
  }
 
}

