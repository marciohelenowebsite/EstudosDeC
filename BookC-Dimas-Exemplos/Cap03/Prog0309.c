#include"stdio.h"

int main() {

  /*
   * Escreva um programa que aplique uma taxa de imposto de 10% aos
   * aos solteiros e de 9% aos casados.
   */

  float salario;
  char estado_civil;

  printf("Qual o seu salário:\n");
  scanf("%f", &salario);
  // printf("\n");
  // printf("---------------------------");

  printf("Qual o seu estado civil?\n");
  scanf("%s", &estado_civil);

  if ( estado_civil == 'C' || estado_civil == 'c') {
    printf("Imposto: %.2f\n", salario * 0.09);
  } else if (estado_civil == 'S' || estado_civil == 's') {
    printf("Imposto: %.2f\n", salario * 0.1);
  } else {
    printf("Estado Civil incorreto!!!\n");
  }

}
