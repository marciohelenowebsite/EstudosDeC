#include"stdio.h"

int main() {

  /*
   * Implemente um programa que calcule os aumentos de ordenado
   * para o corrente ano. Se o ordenado for > 1000 deve ser
   * aumentado 5% se não deve ser aumentado 7%
   */

  float ordenado;
  float aumento;

  printf("Informe o valor do seu ordenado: \n");
  scanf("%f", &ordenado);

  if ( ordenado > 1000) {
    aumento = ordenado + (ordenado * 0.05);
    printf("Seu ordenado teve um aumento de 5%%, o seu valor agora é de %.2f\n", aumento);
  } else {
    aumento = ordenado + (ordenado * 0.07);
    printf("Seu ordenado teve um aumento de 7%%, o seu valor agora é de %.2f\n", aumento);
  }

}
