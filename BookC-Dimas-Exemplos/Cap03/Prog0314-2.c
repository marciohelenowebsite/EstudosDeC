#include "stdio.h"

int main() 
{

  /*
   * Escreva um programa que calcule o imposto pago por mulheres e por homens,
   * sabendo que as mulheres pagam 10% e os homens pagam mais 5% do que as mulheres. 
   */

  float salario;
  float imposto = 0.0;
  char sexo;

  printf("Informe o valor do seu salário. \t\n");
  scanf("%f", &salario);
  printf("Informe o seu sexo. \t\n");
  scanf(" %c", &sexo);

  switch (sexo) 
  {
    case 'f':
    case 'F': 
      imposto = 0.10;
      break;

    case 'm':
    case 'M':
      imposto = 0.15;
      break;
  }

  printf("O imposto cobrado e de %.2f em cima do seu salário de R$ %.2f\t\n", salario * imposto, salario);


}
