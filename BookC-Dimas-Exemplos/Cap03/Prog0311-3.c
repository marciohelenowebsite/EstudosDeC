#include "stdio.h"

int main() {

  char estCivil;
  printf("Qual o Estado Civil: "); 
  scanf(" %c", &estCivil);

  switch(estCivil)
  {
    case 'c':
    case 'C': 
      printf("Casado \n"); 
      break;
    case 's':
    case 'S':
      printf("Solteiro \n");
      break;
    case 'd':
    case 'D':
      printf("Divorciado \n"); 
      break;
    case 'v':
    case 'V':
      printf("Viúvo \n");
    break;
    default: 
      printf("Estado Civil incorreto \n");

  }

}
