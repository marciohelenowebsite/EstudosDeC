#include "stdio.h"

int main () {

  char est_Civil;
  puts("Qual o estado Civil:");
  est_Civil = getchar();

  switch(est_Civil) {

    case 'C' :
      printf("Casado \n");
      break;
    case 'D' :
      printf("Divorciado \n");
      break;
    case 'V': 
      printf("Viúvo \n");
      break;
    case 'S' : 
      printf("Solteiro \n");
      break;
    
    default : 
      printf("Estado civil incorreto \n");
  
  }
 

}
