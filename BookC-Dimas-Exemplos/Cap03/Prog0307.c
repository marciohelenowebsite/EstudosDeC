#include"stdio.h"

int main() {
  /*
   * Escreva um programa que calcule o Salário Bruto,
   * Líquido, e o Imposto a pagar seguido a senguinte 
   * regra:
   * ________________________________________________
   *         Sálario        |         Taxa          | 
   *         < 1000         |           5%          |
   *    >= 1000 e < 5000    |          11%          |
   *        >= 5000         |          35%          |
   * _______________________|_______________________|
   */

  float salario;
  float taxa;

  printf("Programa para calcular seu imposto:\n");
  printf("Informe o valor do seu Salário\n");
  scanf("%f", &salario);

  if (salario < 1000) {
    taxa = 0.05;
  } else if (salario >=1000 && salario < 5000) {
    taxa = 0.11;
  } else {
    taxa = 0.35;
  }

  printf("Salário: %.2f  | Imposto: %.2f   | Liquido:  %.2f\n", salario, salario * taxa, salario * (1.0 - taxa));

  
}
