#include "stdio.h"
#include "stdlib.h"

int x_toupper(char ch);

int main() 
{
  char c;

  while(1)
  {
    c = getchar();
      putchar(x_toupper(c));
  }
  
  system("pause");
  
  return 0;

}

int x_toupper(char ch)
{
  if (ch >= 'a' && ch <='z')
    return ch + 'A' - 'a';
  else
    return ch;
}
