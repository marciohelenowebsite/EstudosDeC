#include "stdio.h"

/*
 * uma função para criar linha sendo possível passar 
 * a quantidade de linhas por parâmetro.
 */

void linha (int num, char ch) {
  int i;
  for (i = 0; i <= num; i++) {
    putchar(ch);
  }
  putchar('\n');
}

int main ()
{
  printf("Informe a quantidade de linhas e caracteres em cada String criada\n");

  linha( 3, '+');
  linha( 5, '+');
  linha( 7, '*');
  linha( 5, '+');
  linha( 3, '+');
}


