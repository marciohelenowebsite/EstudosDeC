#include "stdio.h"

/*
 * Implemente um programa que solicite dois números ao usuário e apresente 
 * na tela o resultado da sua soma e o dobro de cada um deles
 */

double soma(double x, double y)
{
  return x + y;
}

double dobro(double x)
{
  return 2 * x ;
}

double maior(double x, double y)
{
  if (x > y) 
    return x;
  else 
    return y;
}

double isequal(double x, double y)
{
  return ( x == y); 
}

int main() 
{

  double num1, num2;


  printf("Informe dois numeros: \n");
  scanf("%lf%lf", &num1, &num2);

  printf("A soma desse numeros é: %.2lf\t\n", soma(num1, num2));

  printf("O dobro do número %.2lf é %.2lf\n", num1, dobro(num1));

  printf("Resposta do problema (printf \"\%\%lf \", dobro(soma(dobro(2), 3)));\n");
  
  printf("A saida da função acima é: %.2lf\n", dobro(soma(dobro(num1), num2)));

  printf("Uma função que retorna o maior de dois numeros.\n");

  printf("O numero maior entre %.2lf e %.2lf é %.2lf\n", num1, num2, maior(num1, num2));

  printf("Os acima digitados são iguais ou diferentens:\n");
  printf("O dobro de %.2lf e %.2lf são: %.2lf", dobro(num1), num2, isequal(dobro(num1), num2));

}
