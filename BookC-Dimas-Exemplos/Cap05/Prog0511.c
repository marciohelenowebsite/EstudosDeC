#include "stdio.h"

/*
 * Implemente uma função que calcule o valor de xˆn 
 */

float Pot(float x, int n)
{
  int i; 
  float poh;
  for (i = 1, poh = 1.0; i <= n; i++) {
    poh *=x;
  }
  return poh;
}

int main() 
{

  float num1;
  int expoente;
  float result;

  printf("Informe um numero e veja e sua potencia:\n");
  scanf("%f", &num1);
  printf("Informe o numero do seu expoente:\n");
  scanf("%d", &expoente);

  result = Pot(num1, expoente);

  printf("O resultado é: %.2f\n", result);

  printf("_____________________________\n");
  printf("Um outra forma de imprimir na tela.\n");
  printf("%.2f %.2f %.2f\n", Pot(num1, expoente), Pot(5.0, 2), Pot(4.0,3));


}
