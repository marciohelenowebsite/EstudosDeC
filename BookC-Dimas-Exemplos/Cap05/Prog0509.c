#include "stdio.h"

/*
 * Escreva uma função x_isdigit que verifica se um determinado caractere é dígito
 * ou não. Escreva um programa de teste da função.
 */

int x_isdigit(char ch);

int main() 
{
  char c;
  while(1)
  {
    c = getchar();
    if (!x_isdigit(c))
      putchar(c);
  }
}

int x_isdigit(char ch) 
{
  return (ch >= '0' && ch <= '9');
}

