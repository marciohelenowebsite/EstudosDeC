#include "stdio.h"
#include "stdlib.h"
/*
  Escreva um programa que realize a leitura dos salários
  pagos a um indivíduo durante um ano. Em seguida, o
  programa deverá mostrar os valores mensais e o total
  anual.
*/

int main() {

  float sal[12];
  float total;
  int i;

  for (i = 0; i < 12; i++) {
    printf("Digite o salário do mês %d:", i + 1);
    scanf("%f", &sal[i]);
  }

  /* Mostrar os valores mensais */
  
  puts(" Mês        Valor ");
  for (i = 0, total = 0.0; i < 12; i++) {
    printf("%3d     %9.2f\n", i + 1, sal[i]);
    total += sal[i];
  }

  printf("Total Anual: %9.2f\n", total);
   
  system("cls");


  return 0;
}
