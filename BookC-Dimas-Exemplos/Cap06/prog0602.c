#include "stdio.h"

/*
  Escreva um programa que realize a leitura dos salários
  pagos a um indivíduo durante um ano. Em seguida, o
  programa deverá mostrar os valores mensais e o total
  anual.
*/

int main() {

  float sal[13];
  float total;
  int i;

  for (i = 0; i <= 12; i++) {
    printf("Digite o salário do mês %d:", i);
    scanf("%f", &sal[i]);
  }
// mostrar os valores Mensais e calcular o total;
  puts(" Mês        Valor ");
  for (i = 0, total = 0.0; i <= 12; i++) {
    printf("%3d     %9.2f\n", i, sal[i]);
    total += sal[i];
  }

  printf("Total Anual: %9.2f\n", total);

  return 0;
}
