//
//  main.c
//  bigYearX
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>
#define TARGET_AGE 88

int year1, year2;

int clacYear(int year1);

int main(int argc, const char * argv[]) {
    
    printf("Informe o ano que você nasceu.\n");
    printf("Digite o ano do seu nascimento (AAAA):\n");

    scanf("%d", &year1);

    year2 = calcYear(year1);

    printf("Alguém nascido em %d daqui a %d terá %d\n", year1, TARGET_AGE, year2);

    
    
    return 0;
}

int calcYear(int year1)
{
    return (year1 + TARGET_AGE);
}
