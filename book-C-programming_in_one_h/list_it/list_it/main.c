//
//  main.c
//  list_it
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//  Função :: Exibe uma lista com os numeros de linha.
//

#include <stdio.h>
#include <stdlib.h>
#define BUFF_SIZE 256

// inicialização de variaveis
void display_usage(void);
int row;

int main(int argc, const char * argv[]) {

    char buffet[BUFF_SIZE];
    FILE *fp;

    if(argc < 2)
    {
        display_usage();
        return 1;
    }


    if ((fp = fopen(argv[1], "r")) == NULL) {
        fprintf(stderr, "Error opening file, %s!", argv[1]);
        return (1);
    }

    row = (1);

    while (fgets(buffet, BUFF_SIZE, fp) != NULL) {
        fprintf(stdout, "%4d:\t%s", row++, buffet);
    }

    fclose(fp);


    return 0;
}


void display_usage(void)
{
    fprintf(stderr, "\nProper Usage is: ");
    fprintf(stderr, "\n\nlist_it filename.ext\n");
}
