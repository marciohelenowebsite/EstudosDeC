//
//  main.c
//  const
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

#define LAPS_PER_MILE 4

const int CURRENT_YEAR = 2017;

float miles_covered;
int laps_run, year_of_birth, current_age;

int main(int argc, const char * argv[]) {

    /* Input data from user */
    printf("Quantas voltas você correu:\n");
    scanf(" %d", &laps_run);
    printf("Digite seu ano de nascimento\n");
    scanf(" %d", &year_of_birth);

    /* convertendo */
    miles_covered = (float) laps_run/LAPS_PER_MILE;
    current_age = CURRENT_YEAR - year_of_birth;

    /* Display results on the screen */
    printf("Você correu %.2f milhas.\n", miles_covered);
    printf("Nada mal para alguém com %d anos.\n", current_age);

    return 0;
}
