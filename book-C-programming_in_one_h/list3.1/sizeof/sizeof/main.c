//
//  main.c
//  sizeof
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//
//  Programa para dizer o tamanho da variável em C em bytes

#include <stdio.h>

int main(int argc, const char * argv[]) {

    printf("\n A char is %d bytes", sizeof(char));
    printf("\n A int is %d bytes", sizeof(int));
    printf("\n A short is %d bytes", sizeof(short));
    printf("\n A long is %d bytes", sizeof(long));
    printf("\n A long long is %d bytes\n", sizeof(long long));

    printf("\n A unsigned char is %d bytes", sizeof(unsigned char));
    printf("\n A unsigned int is %d bytes", sizeof(unsigned int));
    printf("\n A unsigned short is %d bytes", sizeof(unsigned short));
    printf("\n A unsigned long is %d bytes", sizeof(unsigned long));
    printf("\n A unsigned long long is %d bytes\n", sizeof(unsigned long long));

    printf("\n A float is %d bytes", sizeof(float));
    printf("\n A double is %d bytes\n", sizeof(double));
    printf("\n A long doube is %d bytes\n", sizeof(long double));

    return 0;
}
