/*
 * bigyear.c
 *
 *  Created on: 17 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>
#define TARGET_AGE 88

int year1, year2;
int clacYear(int year1);

int main(void)
{

	printf("Em que ano você nasceu?\n");
	printf("Digite o ano do seu nascimento com 4 digitos (XXXX):\n");
	scanf("%d", &year1);

	year2 = clacYear(year1);

	printf("Se você nasceu em %d, quando tiver %d anos, será o ano de %d.", year1, TARGET_AGE, year2);

	return 0;

}

int clacYear(int year1)
{
	return(year1 + TARGET_AGE);
}
