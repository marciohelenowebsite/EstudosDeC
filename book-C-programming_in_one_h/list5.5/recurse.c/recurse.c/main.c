//
//  main.c
//  recurse.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//  Demostar função recursão.
//  fatorial de um número.

#include <stdio.h>

unsigned int f, x;

unsigned int factorial(unsigned int a);

int main(void) {
    // insert code here...
    puts("Enter an integer value between 1 and 8:");
    scanf("%d", &x);
    
    if (x > 8 || x < 1) {
        printf("Only values from 1 to 8 are accepatable!");
    }else {
        f = factorial(x);
        printf("%u factorial equals %u\n", x, f);
    }
    
    
    return 0;
}

unsigned int factorial(unsigned int a)
{
    if (a == 1) {
        return 1;
    } else {
        a *= factorial(a-1);
        return a;
    }
}
