//
//  main.c
//  escale.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

#define QUIT 3

int get_menu_choice(void);
void print_report(void);

int main(int argc, const char * argv[]) {
    // insert code here...
    
    int choice = 0;
    
    printf("\"We\'d like to welcome you to the menu program\"\n");
    printf("Are you ready to make a choice\?\n");
    
    while (choice != QUIT) {
        choice = get_menu_choice();
        
        if (choice == 1) {
            printf("\n Beeping the computer \a\a\a");
        } else {
            if (choice == 2) {
                print_report();
            }
        }
        
        printf("You chose to quit!\n");
    }
    
    return 0;
}


int get_menu_choice(void)
{
    int selection = 0;
    
    do {
        printf("\n");
        printf("\n 1 - Beep Computer");
        printf("\n 2 - Display");
        printf("\n 3 - Quit");
        printf("\n");
        printf("\nEnter a selection");
        
        scanf("%d", &selection);
    } while (selection < 1 || selection > 3);
    
    return selection;
}

void print_report(void)
{
    printf("\n SAMPLE REPORT");
    printf("\n\n Sequence \t Meaning");
    printf("\n =========\t=======");
    printf("\n\\a\t\tbell(alert)");
    printf("\n\\b\1t\tbell(backspace)");
    printf("\n...\t\t...");
}


