//
//  main.c
//  forstate.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

#define MAXCOUNT 20

int count;


int main(void) {
    
    /* Print the number a through 20 */
    
    for (count = 1; count <= MAXCOUNT; count++) {
        printf("%d\n", count);
    }
    
    return 0;
}
