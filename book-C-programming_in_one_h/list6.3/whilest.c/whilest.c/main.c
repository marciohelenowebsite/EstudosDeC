//
//  main.c
//  whilest.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

#define MAXCOUNT 20
int count;

int main(int argc, const char * argv[]) {
    // insert code here...
    
    count = 1;
    while (count <= MAXCOUNT) {
        printf("%d\n", count);
        count++;
    }
    
    return 0;
}
