//
//  main.c
//  ex02-02
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//  exercicio 2 do livro.
//

#include <stdio.h>

void display_line(void);

int main(int argc, const char * argv[]) {

    display_line();
    printf("\n Teach Yourself C In One Hour a Day!\n");
    display_line();

    printf("\n");

    return 0;
}

void display_line(void)
{
    int counter;
    for (counter = 0; counter < 37; counter++) {
        printf("*");
    }
}
