//
//  main.c
//  nums.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

int a = 2, b = 10, c = 50;
float f = 1.05, g = 25.5, h = -0.1;

int main(int argc, const char * argv[]) {
    printf("\n Decimal values without tabs: %d %d %d", a, b ,c);
    printf("\n Decimal values without tabs: \t%d \t%d \t%d", a, b ,c);
    
    printf("\n Three floats on 1 line: \t%f \t%f \t%f", f, g ,h);
    printf("\n Three floats on 3 line: \n\t%f \n\t\t%f \n\t\t%f", f, g ,h);
    
    printf("\n The rates is %f%%", f);
    
    printf("\n The rate to 2 decimal places is %.2f%%",f);
    printf("\n The rate to 1 decimal places is %.1f%%",f);
    printf("\n The result of %f%f = %f\n", g,f,g / f);
    
    return 0;
}
