#include<stdio.h>

int a, b;

int main(void) 
{

  // Set a and b to o to start 

  a = b = 0;

  // start with the incremental operator 
  // print them, decrementing each time 
  // Use prefix mode for b, postfix mode for a
  //
  printf("Count up!\n");
  printf("Post Pre\n");
  printf("%d     %d\n", a++, ++b);
  printf("%d     %d\n", a++, ++b);
  printf("%d     %d\n", a++, ++b);
  printf("%d     %d\n", a++, ++b); 
  printf("%d     %d\n", a++, ++b); 
  printf("%d     %d\n", a++, ++b);

  printf("\nCurrent values of a and b: \n");
  printf("%d     %d\n\n", a,b);

  printf("Count down!\n");
  printf("\n Post   Pre");
  printf("\n %d     %d", a--, --b);
  printf("\n %d     %d", a--, --b); 
  printf("\n %d     %d", a--, --b); 
  printf("\n %d     %d", a--, --b);
  printf("\n %d     %d", a--, --b);

  return 0;

}
