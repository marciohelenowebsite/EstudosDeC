//
//  main.c
//  var.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

int x = 1, y = 2;

void demo(void);

int main(void) {
    // insert code here...
    
    printf("\n Before calling demo(), x = %d and y = %d\n", x, y);
    demo();
    printf("\n After calling demo(), x = %d and y = %d\n", x, y);
    
    return 0;
}

void demo(void)
{
    /* Declare and initialize two local variables */
    int x = 88, y = 99;
    
    /* Display their values. */
    printf("\n Within demo(), x = %d and y = %d.", x, y);
}
