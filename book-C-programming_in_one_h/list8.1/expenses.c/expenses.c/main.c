//
//  main.c
//  expenses.c
//
//  Created by marcio heleno maia pessoa on 05/10/2017.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

/* Declare an array to hold expenses, and a counter variable */
float expenses[13];
int count;
float year_expenses = 0;


int main(int argc, const char * argv[]) {
    // insert code here...
    for (count = 1; count < 13; count++) {
        printf("Enter expenses for month %d: ", count);
        
        scanf("%f", &expenses[count]);
    }
        
    /*print arrya */
    
    for (count = 1; count < 13; count++)
    {
        printf("Month %d = $%.2f\n", count, expenses[count]);
        year_expenses += expenses[count];
    }

    printf("Yearly expenses are $%.2f\n", year_expenses);
 
    return 0;
}
