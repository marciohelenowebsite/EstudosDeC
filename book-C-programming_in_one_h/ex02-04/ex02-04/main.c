//
//  main.c
//  ex02-04
//
//  Created by marcio heleno maia pessoa on 17/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>

int main(void) {

    int ctr;

    for (ctr = 65; ctr > 91; ctr++) {
        printf("%c", ctr);
    }

    printf("\n");

    return 0;
}
