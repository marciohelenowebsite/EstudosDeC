//
//  main.c
//  pilhaDois
//
//  Created by marcio heleno maia pessoa on 21/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 6

// Implementando a estrutura
typedef struct
{
    int topo, item[MAX_SIZE];
}stack;

// Função  Inicia Pilha
void iniciaPilha ( stack *p)
{
    p->topo = -1; // O topo começa em -1,pq em 0 signicaria que estaria já na primeira posição
}

// Função Verifica se a  pilha está Cheia
int pilhaCheia ( stack *p)
{
    if (p->topo == (MAX_SIZE-1))
    {
        printf ("\n\n\t\tA Pilha esta Cheia!!!");
        return 1;
    }else
        return 0;
}

// Função Verifica pilha Vazia
int pilhaVazia ( stack *p)
{
    if (p->topo == -1)
    {
        printf ("\n\n\t\tA Pilha esta Vazia!!!");
        return 1;
    }
    else
        return 0;
}

//  Função verifica se a 2 está pilha Vazia
int pilhaVazia2 (stack *p)
{
    if (p->topo == -1) // O topo começa em -1,pq em 0 signicaria que estaria já na primeira posição
        return 1;
    else
        return 0;
}

//   Função Empilha
int push ( stack *p, int valor)
{
    return (p->item[++(p->topo)] = valor);// retornar ao valor do topo
}

//  Função Desempilha
int pop ( stack *p)
{
    int aux;
    aux = p->item[(p->topo)--]; //decrementando o topo, ou seja, desempilhando os elementos
    return aux;
}

// Função Mostra Pilha

int mostra (stack *p)
{
    int aux;
    if (pilhaVazia2 (p))
        return 1;
    else
    {
        aux = pop (p);
        printf ("%d,", aux);
        mostra (p);
        return 0;
    }
}


//  Função reempilha
int reempilha ( stack *p,  stack *pPilhaB,  stack *pPilhaC)
{
    int aux;
    if (pilhaVazia (p)&&pilhaVazia (pPilhaB))
        return 1;
    else
    {
        if ((p->item[p->topo]&&(pPilhaB->item[pPilhaB->topo]))
            //se o topo estiver com uma valor reempilha os valores de A em B
        {
                aux = pop (p)&&pop(pPilhaB);
                push (pPilhaC, aux);
                //push (pPilhaB, aux);
         }
            reempilha (p, pPilhaB, pPilhaC); // chamando a função para reempilhar os elementos em
            return 0;
	}

}

// Funcao para pegar os valores a empilhar
int empilha ( stack *p)
{
            int valor;
            printf ("\n\nInforme os seis valores para ser empilhado ou -2 para mostrar os valores de B  ");
            scanf ("%d",&valor);
            if (pilhaCheia (p))
                return 1;
            else
            {
                if (valor == -2)// se o numero -2 for digitado então os valores impares e pares serão colocados
                    return 0;
                else
                {
                    push (p, valor);
                    empilha (p);
                    return 0;
                }
            }
        }
            int ordenar( stack *p,  stack *pPilhaB,  stack *pPilhaC){
                // pilhac=para receber os elementos desempilhados e ordenados
                int aux;//variavel para comparar os elementos

                if (pilhaVazia (p)&&pilhaVazia (pPilhaB))//se a pilha estiver vazia retorno 0, não existe ainda ordenação
                    return 0;
                pop (p)&&pop(pPilhaB);// Vou ter que desempilhar um unico elemento de cada pilha

                if ((p->item[p->topo]>=(pPilhaB->item[pPilhaB->topo])) //se o topo estiver com uma valor reempilha os valores de A em B

                        aux = pop (p)&&pop(pPilhaB);// aux vai receber os elemntos do topo das duas pilhas

                        empilha(pPilhaC);

                    }
                    ordenar (p, pPilhaB, pPilhaC); // chamando a função para reempilhar os elementos em
                    return 1;
                    }



