#include"stdio.h"

int main() {

  /*
   * Implemente um programa que leia três 
   * números inteiros e positivos (A, B e C) e 
   * calcule a seguinte expressão: D = (R+S)/2,
   * onde R = (A+B)2 e S = (B+C)2
   */

  int num1, num2, num3;

  printf("Informe 3 numeros distintos: \t\n");
  scanf("%d%d%d", &num1, &num2, &num3);

  int r = (num1 + num2) * 2;
  int s = (num2 + num3) * 2;
  int d = (r + s) / 2;
  //printf("O valor do primeiro numero digitado é %d\n", num1);
  //printf("O valor do primeiro numero digitado é %d\n", num2);
  //printf("O valor do primeiro numero digitado é %d\n", num3);
  
  printf("O valor da expressão r = num1 + num2 * 2 é %d\n", r);
  printf("O valor da expressão s = num2 + num3 * 2 é %d\n", s);
  
  printf("O valor da expressão D = (R + S) /2 => R = (A+B)2 e S = (B + C)2 = %d\t\n", d);

}
