#include "stdio.h"

/*
 * Escreva um programa em C que solicita ao usuário para digitar
 * um número e mostre-o por extenso. Este número deve variar 
 * entre 1 e 10. Se o usuário introduzir um número que não 
 * está no intervalo, o programa deve mostrar "numero invalido
 */


int main() 
{
  int numUser;

  printf("Informe um número por extenso: esse numero pode variar de 1 a 10.\n");
  scanf("%i", &numUser);
  
  switch(numUser)
  {
    case 1:
      printf("Um\n");
      break;
    
    case 2: 
      printf("Dois\n");
      break;
    
    case 3:
      printf("Três\n");
      break;

    case 4:
      printf("Quatro\n");
      break;
    
    case 5:
      printf("Cinco\n");
      break;
    
    case 6:
      printf("Seis\n");
      break;
    
    case 7:
      printf("Sete\n");
      break;
    
    case 8:
      printf("Oito\n");
      break;

    case 9:
      printf("Nove\n");
      break;

    case 10:
      printf("Dez\n");
      break;

    default: 
      printf("número invalido\n");

  }

}
