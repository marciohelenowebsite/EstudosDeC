#include "stdio.h"

/*
 * escreva um programa que solicite dois números reais 
 * e imprima o maior deles. Use uma função que faça a 
 * comparação dos dois números e retorne o maior deles.
 */

int chekMaior(int x, int y)
{
  if ( x > y) {
    return x;
  } else {
    return y;
  }
}

float chekMaiorFloat(float x, float y)
{
  if (x > y) {
    return x;
  } else {
    return y;
  }
}

int main() 
{

  int num1;
  int num2;

  printf("informe dois números inteiros: \n");
  scanf("%i%i",&num1, &num2);

  printf("O número %i é o maior entre os números digitados.\n", chekMaior(num1, num2));

  printf("___________________________________________________________________________\n");

  float num3;
  float num4;
  printf("Informe dois números reais: \n");
  scanf("%f%f", &num3, &num4);

  printf("O número %.2f é o maior entre os números digitados.\n", chekMaiorFloat(num3, num4));
  
  printf("___________________________________________________________________________\n");

  double num5;
  double num6;
  printf("Informe dois números qualquer: \n");
  scanf("%lf%lf", &num5, &num6);

  printf("O número %.2lf é o maior entre os números digitados.\n", chekMaiorFloat(num5, num6));
  
  printf("___________________________________________________________________________\n");

}

