#include "stdio.h"

/*
 * Escreva um programa que solicite dois
 * números inteiros positivos ao usuário e 
 * imprima na tela a soma de todos os 
 * inteiros que estão no intervalo entre os 
 * números informados 
 */

int main() 
{

  int num1 = 0;
  int num2 = 0;
  int total = 0;
  int i;

  printf("Informe a dois números inteiros distintos sendo o primeiro menor e segundo maior. \t\n");
  scanf("%i%i", &num1, &num2);

  for(i = num1; i <= num2; i++) {
    total = total + i;
  }

  printf("Informe a soma dos números no seu intervalo e de: %i\n", total); 

}
