#include "stdio.h"


int main() 
{
  int num1 = 0;
  int num2 = 0;

  printf("Informe um número. \n");
  scanf("%d", &num1);

  printf("Informe um outro número sendo esse maior que o anterior.\n");
  scanf("%d", &num2);

  int total = 0;
  int i = num1;
  do {
    total = total + i;
    i++;
  } while (i <= num2);

  printf("O valor dos seus intervalos é %i\n", total);


}


