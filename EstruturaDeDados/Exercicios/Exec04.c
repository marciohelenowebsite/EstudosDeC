#include "stdio.h"

/*
 * Um programa que seja solicitado ao usuário apenas um 
 * número inteiro e seja impresso na tela a soma de todos 
 * os números inteiros múltiplos de 3 que estão entre 0 e o 
 * número informado pelo usuário (obs.: utilize a estrutura
 * de repetição for).
*/
int main() {

  int numUsuario = 0;

  printf("Informe um número e mostrarei seus multiplos \t\n");
  scanf("%d", &numUsuario);

  int i;
  int total = 0;
  
  for(i = 0; i <= numUsuario; i++) { 
    if ( i % 3 == 0) {
      total = total + i;
    }
  }

  printf("Esses são os multiplos de %d\t\n", total);

  return 0;
}
