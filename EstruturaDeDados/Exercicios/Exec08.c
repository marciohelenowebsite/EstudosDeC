#include "stdio.h"

/*
 * Escreva uma função que receba o ano como argumento e retorne 1 
 * se for um ano bissexto e 0 se não for. Um ano é bissexto se for
 * divisível por 4, mas não por 100. Um ano também é bissexto se 
 * for divisível por 400.
 */

int bissexto(int ano)
{
  if (ano % 400 == 0) {
    return 1;
  } else if ((ano % 4 == 0) && (ano % 100 != 0)) {
    return 1;
  } else {
    return 0;
  }
}


int main() 
{
 
  int ano;

  printf("Informe um ano qualquer que lhe diremos se ele é ou não bissexto\n");
  scanf("%i", &ano);

  printf("O nao que você digitou ele é: %i\n", bissexto(ano));

}
