#include "stdio.h"

/*
 * Um número e perfeito se ele é igual à soma de seus divisores
 * menores do que ele próprio. Assim, por exemplo, 6 é perfeito,
 * pois 6 = 1 + 2 + 3.
 *
 * escreva um função iterativa denominada EhPerfeito (), que retorne 1
 * se o inteiro positivio recebido como argumento seja perfeito e 0 em
 * caso contrário;
 *
 * excreva um programa que receba um número inteiro maior do que ou igual
 * a zero como entrada e imprima se o número é perfeito ou não. O programa
 * deve terminar quando o usuário introduzir o valor 0.
 */

int ehPerfeito(int num)
{
  int i;
  int soma = 0;
  int v[i];
  for (i = 1; i <= num/2; i++) {
    if (num % i == 0) {
      soma = soma + i;
    }
  }
  if (soma == num) {
    return 1;
  } else {
    return 0;
  }

}

int main()
{

  int numUser;
  // int vetorNumP[];
  printf("Informe um número qualquer mostraremos se ele e um número perfeito.\n");
  scanf("%d", &numUser);
  if (ehPerfeito(numUser) == 1) {
    printf("O numero digitado e perfeito.\n" );
  } else {
    printf("O numero digitado não e perfeito.\n" );
  }

}
