#include<stdio.h>
#include<stdlib.h>

struct no{
  int info;
  struct no *anterior;
};

typedef struct no No;

struct pilha{
  No *topo;
};

typedef struct pilha Pilha;

void inicialize(Pilha *p){
  p->topo = NULL;

}

int isEmpty(Pilha *p){
  if(p->topo == NULL){
    return 1;
  }else{
    return 0;
  }
}

int push(Pilha *p,int dado){
  No *novoNo;
  novoNo =(No*)malloc(sizeof(No));

  if(novoNo != NULL){
    novoNo->info = dado;
    novoNo->anterior = p->topo;
    p->topo = novoNo;
    return 1;
  }else{
    return 0;
  }
}

int* pop(Pilha *p){
  No *noRemovido;
  int *dadoRemovido;

  if(isEmpty(p) == 1){
    return NULL;
  }else{
    noRemovido = p->topo;
    dadoRemovido = (int*)malloc(sizeof(int));
    *dadoRemovido = p->topo->info;
    p->topo = p->topo->anterior;
    free(noRemovido);
    return dadoRemovido;
  }
}

int* top(Pilha *p){
  if(isEmpty(p) == 1){
    return NULL;
  }else{
    return &(p->topo->info);
  }
}

int list(Pilha *p){
  int i;
  if(isEmpty(p) == 1){
    return 0;
  }else{
    printf("\nElementos da Pilha:\n\n");
    No *noAux;
    noAux = p->topo;
    while(noAux != NULL){
      printf("%d\n",noAux->info);
      noAux = noAux->anterior;
    }
    return 1;

  }

}

