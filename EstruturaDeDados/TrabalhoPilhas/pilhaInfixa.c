/* This program converts an infix expression to a postfix expression   */

#include <stdio.h>

struct operator { /* operator declaration*/
  char opName;
  int isp;
  int icp;
};
int push (struct operator stak[], int *top, struct operator val, int size);
struct operator pop (struct operator stak[], int *top);
void display (struct operator stack[], int top);
int match(struct operator opTab[8], char element, struct
    operator *op);

int main()
{
  char infix[20];
  char target[20];
  struct operator stack[20]; /* the stack declaration */
  int top = −1; /* initially the stack is empty */
  int res;
  char val;
  int size;
  int pos;
  int i;
  struct operator op, opTemp;
  struct operator opTab[8] = {"(", 0, 6,   /* The operators
                                              information */
    ")", 0, 0,
    "^", 4, 5,
    "*", 3, 3,
    "/", 3, 3,
    "+", 2, 2,
    "−", 2, 2,
    ";", 0, −1,
  };

  printf ("\n Enter the size of the infix expression");
  scanf ("%d", &size); size--;
  printf ("\n Enter the terms of infix expression one by one");
  for (i= 0; i <= size; i++)
  {fflush(stdin); /* flush the input buffer */
    scanf ("%c", &infix[i]);
  }
  pos = 0; /* position in target expression */
  for (i=0; i <= size; i++)
  {
    res = match(opTab, infix[i], &op); /* find whether operator/operand */
    if (res==0)
    {target[pos] = infix[i]; /* store into target expression */
      pos++;
    }
    else
    {if (top < 0)
      push (stack, &top, op, size); /* first time Push */
      else
      {opTemp.opName="#"; /* place any value to opTemp */

        if (op.opName == ")"}}}}
            { while (opTemp.opName != "(" )
            {
            opTemp = pop (stack, &top);
            if (opTemp.opName != "(" ) /* omit "(" */
            { target [pos] = opTemp.opName;
            pos++;
            }
           ))}
            )}

  else
{while (stack[top].isp >= op.icp && top >=0)
  {
    opTemp = pop (stack, &top);
    target [pos] = opTemp.opName;
    pos++;
  }
  push(stack, &top, op, size);
}
}
}
}
/* print the postfix expression */
printf ("\n The postfix expression is:");
for (i = 0; i < pos; i++)
{
  printf ("%c", target[i]);
}
}

int push (struct operator stack[], int *top, struct operator val, int size)
{
  if ( *top >= size)
    return 0; /* the stack is full */
  else
  {
    *top= *top + 1;
    stack[*top]= val;
    return 1;
  }
}
struct operator pop (struct operator stack[], int *top)

{struct operator val;
  if (*top < 0)
  {val.opName ="#";
    return val; /* the stack is empty */
  }
  else

  {
    val = stack[*top];
    *top = *top − 1;
    return val;
  }
}

void display (struct operator stack[], int top)
{
  int i;
  printf ("\n The contents of stack are:");
  for (i = top; i >=0; i--)
    printf("%c ", stack[i].opName);
}
int match(struct operator opTab[8], char element, struct operator*op)
{
  int i;
  for (i =0; i <8; i++)
  {
    if (opTab[i].opName == element)
    {
      *op = opTab[i];
      return 1;
    }
  }
  return 0;
}
