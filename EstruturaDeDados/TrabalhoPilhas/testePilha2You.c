#include<stdio.h>
#include<stdlib.h>
#include "PilhaDinamica.c"

int main(int argc, char *argv[]){
  Pilha p;
  Pilha p1;
  Pilha altern;
  int opcao, res;
  char num;

  inicialize(&p);
  inicialize(&p1);
  inicialize(&altern);

  do{
    printf("Informe uma das opções abaixo:\n");
    printf("1-Inserir na pilha\n");
    printf("2-Remover da pilha\n"); 
    printf("3-Verificar o topo\n"); 
    printf("4-Listar pilha\n");
    printf("5-Verificar os elementos repetidos. \n");
    printf("6-Sair\n");
    printf("\n\nDigite sua opção:");
    scanf("%d",&opcao);

    switch(opcao){
      case 1:
        printf("Informe um caractere:\n");
        scanf(" %c", &num);
        res = push(&p,num);
        if(res == 0){
          printf("\nEspaço não alocado!\n");
        }else{
          printf("\nCaractere %c inserido com sucesso!\n",num);
        }
        break;
      case 2:
        aux = pop(&p);
        if(aux == NULL){
          printf("\nPilha vazia!\n");
        }else{
          num = *aux;
          printf("\nCaractere %c removido da pilha com sucesso!\n", num);
        }
        break;
      case 3:
        aux = top(&p);
        if(aux == NULL){
          printf("\nPilha vazia!\n");
        }else{
          printf("\nElemento do topo = %c\n",*aux);
        }
        break;
      case 4:
        res = list(&p);
        if(res == 0){
          printf("\nPilha vazia!\n");
        }
      
        break;
      case 5:
        printf("Verifica Repetidos na Pilha:\n");
        res = listRepet(&p, &p1, &altern);
        //aux = top(&p);
        //ver = aux;
        //printf("\nElemento do topo = %c\n",*ver);
        break;

      case 6:
        printf("Fim do programa!\n");

      default:
        printf("Opção invalida!Tente novamente!\n");

    }

  }while(opcao!=6);

  return 0;

}
