#include<stdio.h>
#include<stdlib.h>

typedef struct {
  int info;
  struct no *anterior;
}No;

typedef struct {
  No *topo;
}Pilha;

void inicialize(Pilha *p){
  p->topo = NULL;
}

int isEmpty(Pilha *p){
  if(p->topo == NULL){
    return 1;
  }else{
    return 0;
  }
}

int push(Pilha *p, char dado){
  No *novoNo;
  novoNo = (No*)malloc(sizeof(No));
  if(novoNo != NULL){
    novoNo->info = dado;
    novoNo->anterior = p->topo;
    p->topo = novoNo;
    return 1;
  }else{
    return 0;
  }
}

int* pop(Pilha *p){
  No *noRemovido;
  int *dadoRemovido;

  if(isEmpty(p) == 1){
    return NULL;
  }else{
    noRemovido = p->topo;
    dadoRemovido = (int*)malloc(sizeof(int));
    *dadoRemovido = p->topo->info;
    p->topo = p->topo->anterior;
    free(noRemovido);
    return dadoRemovido;
  }
}

int* top(Pilha *p){
  if(isEmpty(p) == 1){
    return NULL;
  }else{
    return &(p->topo->info);
  }
}

int list(Pilha *p){
  //int i;
  if(isEmpty(p) == 1){
    return 0;
  }else{
    printf("\nElementos da Pilha:\n\n");
    No *noAux;
    noAux = p->topo;
    while(noAux != NULL){
      printf(" %c\n",noAux->info);
      noAux = noAux->anterior;
    }
    return 1;
  }

}

int listRepet(Pilha *p, Pilha *pAux, Pilha *pAux2) {
  int aux;
  No *noAux;
  noAux = p->topo;

  if(isEmpty(p) == 1){
    printf("A Pilha estar vazia.\n");
    return 0;
  }else {

    top(p); 
    int *aux2 = top(p);
    printf("Colocando em ordem os elementos da Pilha.\n");

    while(noAux != NULL){

      //printf("Colocando em ordem os elementos da Pilha.\n");

      printf("Quem é vc aux2 %c\n", *aux2);
      int *aux = p->topo;
      printf("O valor de p->topo é de %c\n", *p->topo);
      printf("O valor de aux é de %c\n", *aux);

      printf(" %c\n",noAux->info);
      // noAux = noAux->anterior;

      if (aux2 == aux) {
        printf("São iguais.\n");
        printf("Quem é o topo. %c\n", *p->topo);
        printf("Elemento fora da pilha\n"); 
        pop(p);
      }else {
        printf("Não são iguais\n");
        push(pAux, noAux->info);

      }
      //push(pAux, noAux->info);
      //list(pAux);

      printf(" %c\n",noAux->info);
      noAux = noAux->anterior; 
    }
    //return 1;
  }
  list(pAux);
  
  
  int auxi;
  No *noAux2;
  noAux2 = p->topo;


  if(isEmpty(pAux) == 1){
    printf("A Pilha estar vazia.\n");
    return 0;
  }else {

    top(pAux); 
    int *aux2 = top(pAux);
    printf("Colocando em ordem os elementos da Pilha.\n");

    while(noAux2 != NULL){

      //printf("Colocando em ordem os elementos da Pilha.\n");

      printf("Quem é vc aux2 %c\n", *aux2);
      int *aux = pAux->topo;
      printf("O valor de p->topo é de %c\n", *pAux->topo);
      printf("O valor de aux é de %c\n", *aux);

      printf(" %c\n",noAux2->info);
      // noAux = noAux->anterior;

      if (aux2 == aux) {
        printf("São iguais.\n");
        printf("Quem é o topo. %c\n", *pAux->topo);
        printf("Elemento fora da pilha\n"); 
        pop(p);
      }else {
        printf("Não são iguais\n");
        push(pAux, noAux2->info);

      }
      //push(pAux, noAux->info);
      //list(pAux);

      printf(" %c\n",noAux2->info);
      noAux2 = noAux2->anterior; 
    }
    //return 1;
  }
  list(pAux2);

  return 1;
}
