#include<stdio.h>
#define TAM 5

struct pilha{
  int elementos[TAM];
  int topo;
};
typedef struct pilha Pilha;

void inicialize(Pilha *p){
  p->topo = -1;
}

int isEmpty(Pilha *p){
  if(p->topo == -1){
    return 1;
  }else{
    return 0;
  }
}

int isFull(Pilha *p){
  if(p->topo == TAM - 1){
    return 1;
  }else{
    return 0;
  }
}

int push(Pilha *p,int novo){
  if(isFull(p) == 1){
    return 0;
  }else{
    p->topo++;
    p->elementos[p->topo] = novo;
    return 1;
  }
}
int* pop(Pilha *p){
  int *removido;
  if(isEmpty(p) == 1){
    return NULL;
  }else{
    removido = &p->elementos[p->topo];
    p->topo--;
    return removido;
  }
}

int* top(Pilha *p){
  if(isEmpty(p) == 1){
    return NULL;
  }else{
    return &p->elementos[p->topo];
  }
}

int list(Pilha *p){
  int i;
  if(isEmpty(p) == 1){
    return 0;
  }else{
    printf("\nElementos da Pilha:\n\n");
    for(i=p->topo;i>=0;i--){
      printf("%d\n",p->elementos[i]);
    }
    return 1;
  }

}

