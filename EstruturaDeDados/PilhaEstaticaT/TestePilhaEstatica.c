#include "PilhaEstatica.c"

int main(){
  Pilha p;
  int opcao, res, num;
  int *aux;

  inicialize(&p);

  do{
    printf("Informe uma das opções abaixo:\n");
    printf("\t 1-Inserir na pilha\n");
    printf("\t 2-Remover da pilha\n");
    printf("\t 3-Verificar o topo\n");
    printf("\t 4-Listar pilha\n");
    printf("\t 5-Sair\n");
    printf("\t Digite sua opções:\n");
    scanf("%d",&opcao);


    switch(opcao){
      case 1:
        printf("Informe um inteiro:");
        scanf("%d",&num);
        res = push(&p,num);
        if(res == 0){
          printf("\nPilha cheia!\n");
        }else{
          printf("\nInteiro %d inserido com sucesso!\n",num);
        }
        break;
      case 2:
        aux = pop(&p);
        if(aux == NULL){
          printf("\nPilha vazia!\n");
        }else{
          printf("\nNumero %d removido da pilha com sucesso!",*aux);
        }
        break;
      case 3:
        aux = top(&p);
        if(aux == NULL){
          printf("\nPilha vazia!\n");
        }else{
          printf("\nElemento do topo = %d",*aux);
        }
        break;
      case 4:
        res = list(&p);
        if(res == 0){
          printf("\nPilha vazia!\n");
        }
        break;
      case 5:
        printf("Fim do programa!");
      default:
        printf("Opção invalida!Tente novamente!");

    }

  }while(opcao!=5);

  return 0;
}
