//
//  main.c
//  imprimindoPonteiro
//
//  Created by marcio heleno maia pessoa on 21/09/17.
//  Copyright © 2017 marcio heleno maia pessoa. All rights reserved.
//  O programa a seguir exibi um endereço de memória.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {

    int valor2;
    int valor1;
//0x7fff5fbff62c
    printf("O endereço da variável valor2 é %p\n", &valor2);
    printf("O endereço da variável valor1 é %p\n", &valor1);
//    printf("\033[44m");

    // operador Condicional ?
    int num1 = 3;
//    int num2 = 4;

    int result = (num1 >= 3) ? 1 : 0;

    printf("O resultado é %d\n", result);



    return 0;
}
