/*
 * op.structs.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>
#include <stdlib.h>


typedef struct Book
{
	char title[80];
	int pages;
} Book;

void add(Book books[], int *id);
void display(Book books[], int *id);

int main ()
{
	int op;
	int id = 0;
	Book data[20];

	do {
		printf("\n 1 - New :");
		printf("\n 2 - Display all :");
		printf("\n 3 - Exit :");
		scanf(" %d", &op);

		switch (op) {
			case 1:
				add(data, &id);
				break;

			case 2:
				display(data, &id);
				break;

			default:
				exit(0);
				break;
		}
	} while (op != 0);

	return 0;
}


void add(Book books[], int *id ) {
	printf("New Record \n\n");

	printf("Title book: \n");
	// todo observar que o copilador não aceitou o &
	scanf("%s", books[*id].title);
	printf("Pages: \n");
	scanf("%d", &books[*id].pages);

	*id += 1;
}

void display(Book books[], int *id) {
	for (int i = 0; i < *id; i++) {
//		printf("_________n");
		printf("\n Title: %s \n", books[i].title);
		printf(" Pages: %d \n", books[i].pages);
		printf("__________\n");
	}
}
