/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>
#include <string.h>
#include "struct.c"

int main ()
{
	struct Book book1;
	strcpy(book1.title, "Biblia in C");
	strcpy(book1.author, "Author 1");
	book1.page = 130;

	printf("Titulo:  %s \n", book1.title);
	printf("Author:  %s \n", book1.author);
	printf("Page:  %d \n", book1.page);

	return 0;
}

