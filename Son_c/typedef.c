/*
 * typedef.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>
#include <string.h>

int main(){

	typedef int myInt;
	typedef float myFloat;
	typedef char String[50];

	myInt a = 500;
	myFloat b = 21.2;
	String name;

	strcpy(name, "Márcio");

	printf(" %s\n", name);
	printf(" %d\n", a);
	printf(" %f\n", b);
}
