/*
 * op_arr.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>

float average(int arr[], int size);

int main()
{
	int size;
	int arr[5];
//	float average;
	float average_result;

	printf("Size of array: \n");
	scanf(" %d", &size);

	if (size > 0 && size <= 5) {
		average_result = average(arr, size);
		printf("%.2f", average_result);
	}else {
		printf("Invalid size array");
	}
	return 0;
}

float average(int arr[], int size) {
	float sum;
	float average;
	for (int i = 0; i < size; i++) {
		printf("Enter a number: ");
		scanf(" %d", &arr[i]);
		sum = sum + arr[i];
	}

	average = sum / size;

	return average;
}
