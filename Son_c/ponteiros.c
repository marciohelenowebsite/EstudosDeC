/*
 * ponteiros.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>
#include <stdlib.h>

int main() {
	int value = 100;
	// ponteiro
	int *ptr;

	ptr = &value;


	printf("Value : %d \n", value);
	printf("Memory value: %x \n", value);
	printf("Memory ptr : %x \n", *ptr);


	return 0;
}

