/*
 * mult_arr.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */
#include<stdio.h>

void display(int arr[2][2]);

int main()
{
	int arr[2][2];

	printf("Enter number (4): \n");

	// pega os elementos digitados
	for (int i = 0; i < 2; i++) {
		for(int j = 0; j < 2; j++){
			scanf("%d", &arr[i][j]);
		}
	}

	printf("_______________\n");

	// imprime na tela
//	for (int i = 0; i < 2; i++) {
//		for(int j = 0; j < 2; j++){
//			printf("%d\n", arr[i][j]);
//		}
//	}

	display(arr);
	return 0;
}

void display(int arr[2][2]) {
	// imprime na tela
	for (int i = 0; i < 2; i++) {
		for(int j = 0; j < 2; j++){
			printf("%d\n", arr[i][j]);
		}
	}
}
