/*
 * struc.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 *      Conceito basico de Struct
 *      São guardados um estrutura de dados.
 */
#include <stdio.h>
#include <string.h>

typedef struct Book
{
	char title[80];
	char author[50];
	int page;
} book;

//typedef struct Book book;

int main()
{

	book book1;

	strcpy(book1.title, "Biblia in C");
	strcpy(book1.author, "Author 1");
	book1.page = 130;

	printf("Titulo:  %s \n", book1.title);
	printf("Author:  %s \n", book1.author);
	printf("Page:  %d \n", book1.page);

	return 0;
}

