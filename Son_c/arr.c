/*
 * arr.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */
#include <stdio.h>

int main(int argc, char **argv) {

	// array

	int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

	printf(" %d", arr[0]);
	printf(" %d", arr[1]);
	printf(" %d", arr[2]);
	printf(" %d", arr[3]);
	printf(" %d", arr[4]);
	printf(" %d", arr[5]);
	printf(" %d", arr[6]);
	printf(" %d", arr[7]);
	printf(" %d", arr[8]);
	printf(" %d", arr[9]);

	printf(" \n");
	arr[4] = arr[4] * 2;
	printf("\n %d", arr[4]);

	printf("\n Alterando os valores atraves de um for.");
	for (int i = 0; i < 10; i++) {
		arr[i] = (i + 1) * 2;
		printf("\n %d", arr[i]);
	}

	return 0;

}


