#include "PilhaDinamica.c"

int main(){
   Pilha p;
   int opcao, res, num;
   int *aux;

   inicialize(&p);

   do{
      printf("Informe uma das opcoes abaixo:\n");
      printf("1-Inserir na pilha\n2-Remover da pilha\n3-Verificar o topo\n4-Listar pilha\n5-Sair");
      printf("\n\nDigite sua opcao:");
      scanf("%d",&opcao);

      switch(opcao){
         case 1:
            printf("Informe um inteiro:");
            scanf("%d",&num);
            res = push(&p,num);
            if(res == 0){
               printf("\nEspaco nao alocado!\n");
            }else{
               printf("\nInteiro %d inserido com sucesso!\n",num);
            }
            break;
         case 2:
            aux = pop(&p);
            if(aux == NULL){
              printf("\nPilha vazia!\n");
            }else{
              printf("\nNumero %d removido da pilha com suceso!",*aux);
            }
            break;
         case 3:
            aux = top(&p);
            if(aux == NULL){
              printf("\nPilha vazia!\n");
            }else{
              printf("\nElemento do topo = %d",*aux);
            }
            break;
         case 4:
            res = list(&p);
            if(res == 0){
               printf("\nPilha vazia!\n");
            }
            break;
         case 5:
            printf("Fim do programa!");
         default:
            printf("Opcao invalida!Tente novamente!");

      }

   }while(opcao!=5);

   return 0;


}
