/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */
#include <stdio.h>

int main()
{
	int numero;
	int divisor = 0;
	int i = 0;

	printf("Informe um número: \n");
	scanf(" %d", &numero);

	for (i = 1; i <= numero; i++) {
//		printf(" %d", i);
		if (numero % i == 0) {
			divisor += 1;
		}

	}

	printf("Total de divisores: %d\n", divisor);

	return 0;


}

