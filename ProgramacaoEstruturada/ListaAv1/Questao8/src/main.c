/*
 * main.c
 *
 *  Created on: 24 de set de 2017
 *      Author: marcioheleno
 */
// Questão 8.
// em uma pesquisa de campo, uma editora solicitou os seguintes dados para os
// entrevistados: sexo, idade e quantidade de livros que leu no ano de 2006.
// Faca um programa em c que leia os dados digitados pelo usuário, sendo que
// deverão ser solicitados dados até que a idade digitada seja um valor negativo.

#include <stdio.h>
#include <stdlib.h>

int main() {
	char sexo;
	int idade = 1;
	int totalDeLivros = 0;
	int livrosLidos10 = 0;
	int mulheresLivro = 0;
	int homensLivro = 0;
	int entrevistados = 0;
	int homensLivroIdade = 0;
	float homensMedia = 0;
	int contburros=0;
	double leituraZero = 0;


	while (idade!= 0) {
		printf("Informe a sua idade:\n");
		scanf("%d", &idade);


		if(idade!=0) {
			printf("Informe a quantidade de livros que voce leu em 2006:\n");
			scanf("%d", &totalDeLivros);
			printf("Informe o seu sexo (F - f) OU (M - m) :\n");
			scanf("%s", &sexo);

			entrevistados++;
			if (idade < 10) {
				livrosLidos10 += totalDeLivros;
			}


			if ((sexo=='F'|| sexo == 'f') && (totalDeLivros >= 5)) {
				mulheresLivro++;
			}

			if ((sexo=='M'||sexo=='m') && (totalDeLivros < 5)) {
				homensLivro++;
				homensLivroIdade += idade;

			}
			if (totalDeLivros == 0) {
				leituraZero++;
			}
		}

	}

	if( homensLivro != 0) {
		homensMedia = homensLivroIdade / homensLivro;
	}

	leituraZero = (leituraZero / entrevistados) * 100.0f;

	printf("A quantidade total de livros lidos pelos entrevistados menores de 10 anos: %d\n", livrosLidos10);
	printf("A quantidade de mulheres que leram 5 livros ou mais: %d\n", mulheresLivro);
	printf("A media de idade dos homens que leram menos de 5 livros: %.0f\n", homensMedia);
	printf("O percentual de pessoas que nao leram livros: %.2f\n", leituraZero);




	return 0;

}
