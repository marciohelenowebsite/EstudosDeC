/*
 ============================================================================
 Name        : Questao10.c
 Author      : Márcio Heleno
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main() {

	float nota1;
	float nota2;
	float media;
	int sistemaAberto = 1;

	while(sistemaAberto == 1) {

		printf("Informe a nota da sua AV1: \n");
		scanf("%f", &nota1);

		if((nota1 >= 0) && (nota1 <= 10 )) {
			//			printf("Nota valida");
		}else {
			printf("Nota Invalida");
			return 0;
		}

		printf("______________________\n");
		printf("Informe a nota da sua AV2: \n");
		scanf("%f", &nota2);


		media = (nota1 + nota2) / 2;

		printf("Sua media semestral e de %.2f\n", media);

		printf("Novo Calculo: 1.Sim 2.Nao. \n");
		scanf("%d", &sistemaAberto);
	}

	return 0;
}
