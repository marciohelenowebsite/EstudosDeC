/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include "stdio.h"

void main()
{

  int a = 0;
  int b = 1;
  int auxiliar;
  int i = -1;
  int n;

  printf("Digite um número: ");
  scanf("%d", &n);
  printf("A série desse numero na sequencia de Fibonacci é:\n");
  printf("%d\n", a);
  printf("%d\n", b);

  for(i = 0; i < n; i++)
  {
    auxiliar = a + b;
    a = b;
    b = auxiliar;

    printf("%d\n", auxiliar);
  }
}
