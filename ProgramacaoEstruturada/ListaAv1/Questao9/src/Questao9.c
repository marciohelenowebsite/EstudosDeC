/*
 ============================================================================
 Name        : Questao9.c
 Author      : Márcio Heleno
 Version     :
 Copyright   : Your copyright notice
 Description : Questao 8
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {

	char nome[30];
	int sexo;
	float salario;
	float salarioM = 0;
	float salarioF = 0;
	double mediaSalarioM;
	double mediaSalarioF;
	int qtdFuncM = 0;
	int qtdFuncF = 0;
	float folhaPagamento;
	char menu[3] = "FIM";


	while (strcmp(menu,"FIM") != 0) {
		printf("\t\t\nCadastro da Empresa do Sr. Bits \n");
		printf("Cadastro de funcionário. \n");
		// cadastrar funcionario.
		printf("Informe o primeiro nome do funcionario: \n");
		scanf(" %s", &nome);
		//			printf(" %s", nome); // debug
		printf("Informe seu sexo (M = 1) ou (F = 0): \n");
		scanf(" %d", &sexo);
//		printf(" %c", sexo); // debug
		printf("Informe seu Salario: \n");
		scanf(" %f", &salario);
		//			printf(" %.2f", salario); // debug

		// salario masculino
		if (sexo == 1) {
//			printf("Aqui \n");
			qtdFuncM += 1;
//			printf("%d\n", qtdFuncM);
			salarioM = salarioM + salario;
//			printf("%.2f\n", salarioM);
			mediaSalarioM = salarioM / qtdFuncM;
		}
		// salario feminino
		if (sexo == 0) {
//			printf("Aqui \n");
			qtdFuncF += 1;
//			printf("%d\n", qtdFuncF);
			salarioF = salarioF + salario;
//			printf("%.2f\n", salarioF);
			mediaSalarioF = salarioF / qtdFuncF;
		}
		folhaPagamento = salarioF + salarioM;

		printf("\tPara sair digite FIM: \n");
		scanf(" %s", &menu);
	}
//	printf("fora 1 \n");
//	printf(" %c", sexo); // debug
	// respostas
	// media do salario masculino

	printf("A media do salario do sexo masculino é de %.2f \n", mediaSalarioM);
	printf("A media do salario do sexo feminino é de %.2f \n", mediaSalarioF);
	printf("A Quantidade de funcionario do sexo masculino é de %d \n", qtdFuncM);
	printf("A Quantidade de funcionario do sexo feminino é de %d \n", qtdFuncF);
	printf("O total da folha de pagamento é de %.2f \n", folhaPagamento);
	return 0;
}
