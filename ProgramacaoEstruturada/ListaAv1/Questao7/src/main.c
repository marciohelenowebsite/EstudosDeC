/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

int main ()
{
	//	Escreva um programa em c para uma empresa que decide
	//	dar um resjuste a seus 584 funcionários de acordo com
	//	os seguintes critérios.

	//	50% para aqueles que ganham menos do que três salários minimos;
	//	20% para aqueles que ganham entre três até dez salários mínimos;
	//	15% para aqueles que ganham acima de dez até vinte sálarios minimos;
	//	10% para do demais funcionarios.
	//	Salário Minimo de 937,00

	int sair = 0;
	int opcao;
	float salarioMinimo = 937.00;
	float salario;

	do
	{
		printf("Programa de Ajuste salárial: \n");

		printf("0. Sair\n");
		printf("1. Você ganha ate 3 salários minimos. \n");
		printf("2. Você ganha mais de 3 salários e menos de 10 salarios. \n");
		printf("3. Você ganha mais de 10 salários e menos de 20 salarios. \n");
		printf("4. Para os demais funcionarios que não estão acima. ");

		printf("Opcao: ");
		scanf("%d", &opcao);

		switch(opcao)
		{
		case 0:
			printf("Saindo do menu...\n");
			sair = 1;
			break;
		case 1:
			printf("Digite e seu salario: \n");
			scanf(" %f", &salario);
			printf(" %.2f", salario);
			if(salario <= (salarioMinimo * 3)) {
				salario += salario * 0.50;
				printf("Seu salário ajustado é de %.2f \n", salario);
			}
			printf("Obrigado");
			printf("\n");
			break;
		case 2:
			printf("Digite e seu salario: \n");
			scanf(" %f", &salario);
			printf(" %.2f", salario);
			if((salario > salarioMinimo * 3) && ( salario > salarioMinimo * 10)) {
				salario += salario * 0.20;
				printf("Seu salário ajustado é de %.2f \n", salario);
			}else {
				printf("Digite seu salário correto. \n");
			}
			printf("Obrigado");
			printf("\n");
			break;
		case 3:
			printf("Digite e seu salario: \n");
			scanf(" %f", &salario);
			printf(" %.2f", salario);
			if(salario > salarioMinimo * 10) {
				salario += salario * 0.20;
				printf("Seu salário ajustado é de %.2f \n", salario);
			}else {
				printf("Digite seu salário correto. \n");
			}
			printf("Obrigado");
			printf("\n");
			break;
		case 4:
			printf("Digite e seu salario: \n");
			scanf(" %f", &salario);
			printf(" %.2f", salario);
			if(salario > salarioMinimo * 10) {
				salario += salario * 0.10;
				printf("Seu salário ajustado é de %.2f \n", salario);
			}else {
				printf("Digite seu salário correto. \n");
			}
			printf("Obrigado");
			printf("\n");
			break;
		default:
			printf("Opcao invalida! Tente novamente.\n");
		}


	} while (sair == 0);
}

