/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */

#include <stdio.h>

int main()
{
	int numerado = 1;
	int denominador = 10;

	float soma = 0;

	while(numerado <= 10)
	{
		soma += (pow(2,numerado++))/denominador--;
	}

	printf("%.2f\n", soma);

	return 0;

}
