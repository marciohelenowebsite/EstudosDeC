/*
 * main.c
 *
 *  Created on: 23 de set de 2017
 *      Author: marcioheleno
 */


#include <stdio.h>
int main()
{
	//variaveis
	register int i;
	int soma=0;
	//inicio
	for(i=50;i<=150;i++)
	{
		if (i%2==0)
		{
			soma = soma + i;
		}
	}
	printf("\nSoma dos numeros pares entre 15 a 100: %d\n",soma);
	return 0;
}
